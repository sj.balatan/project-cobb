from quote import Quote

def test_get_all_quotes():
    quote = Quote()
    quote_list = quote.get_all_quotes()
    registered_quotes = [
        'Coffee is a language in itself.',
        'Chances everyone, everywhere',
    ]
    for quote in quote_list:
        assert quote['message'] in registered_quotes

