from flask import Flask, jsonify
from quote import Quote

app = Flask(__name__)

@app.route('/')
def home():
    return 'RANDOM QUOTE by isji'

@app.route('/quote')
def get_random_quote():
    quotes = Quote()
    random_quote = quotes.get_random_quote()
    return jsonify({'quote' : random_quote['message'], 'author' : random_quote['author']})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
