class Quote:
    def __init__(self):
            self.quotes_list = [
                {
                    'message': 'Coffee is a language in itself.',
                    'author': 'Jackie Chan'
                },
                {
                    'message': 'Chances everyone, everywhere',
                    'author': 'RareJob'
                },
            ]        
    def get_random_quote(self):
        import random    
        return random.choice(self.quotes_list)
    def get_all_quotes(self):
        return self.quotes_list

# FOR TESTING
# quote = Quote()
# random = quote.get_random_quote()
# print(random)