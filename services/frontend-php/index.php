<?php
    //Call to API
    $handle = curl_init();
    $url = "http://service-quotes:5000/quote";
    curl_setopt($handle, CURLOPT_URL, $url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
    $data = curl_exec($handle);
    curl_close($handle);
    $result = json_decode($data);
    
    //Display result of API call
    print "<h1 style='color:black;'>";
    print "RANDOM QUOTE GENERATOR";
    print "</h1>";

    print "<h2 style='color:orange;'>\"";
    print $result->{'quote'};
    print "\"</h2>";

    print "<h3 style='color:gray;'>";
    print "- ".$result->{'author'};
    print "</h3>";