**PROJECT COBB**

Overview
* Why? To be more comfortable and familiar with DevOps. 
* How? By building a simple software delivery system from scratch
* What? Create a simulation of Continuous Delivery

Technology/Tools:
* Python - Backend programming language
* Gitlab - Code Versioning
* Docker - Development Platform
* GitLab CI/CD - CI Server
* Pytest - Unit Tests
* Pylint - Lint Test
* Deployment - AWS Lambda (Not yet implemented)

Why Project Cobb? 
* Cobb is the name of Leonardo Dicaprio's character in Inception (movie). He's one of our, my wife and I, favorite portrayals of Leonardo. In every stage of his career, he’s awesome. Hence, I would like to create a sample CI/CD that has awesomeness in every stage. 

Application (2 services)
1.  Quotes Service: Generates random quotes. (Docker: Python + Alpine)
2.  Frontend Service: Displays quotes. (Docker: Docker + Apache)

Machine Requirements
1.  Git 
2.  Docker

Build & Run 
* `git clone git@gitlab.com:sj.balatan/project-cobb.git`
* `docker-compose up -d --build`